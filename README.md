
Installationsinstruktioner TPK
==============================

Installationen sker med ansible-playbook.   Installationsskripten kan hämtas från gitlab:

https://gitlab.com/TPK-projekt/tpk-ansible.git

Notera att på grund av denna bugg i ansible krävs minst Ansible version 2.6:

https://github.com/ansible/ansible/issues/42180

## Förberedelser

Klona git-repositoriet.

### Provisionering

Installationsskripten delas in i 5 olika roller.  Alla 5 roller kan
hanteras av samma maskin eller olika maskiner.  Det går dock att installera de olika
rollerna på olika maskiner.

Rollerna är:

* webservers - Webbserver som kör Apache.  Om flera behöver de frontas av en lastbalanserare.
* applicationservers - Applikationsserver som kör XWiki ovanpå Apache Tomcat.  Här kan flera maskiner köras i ett kluster.
* databaseservers - PostgreSQL-server.
* buildserver - En maskin för att kompilera programvara.
* deployserver - Sjösätter innehåll från test-miljön och initierar databas.

Maskinerna skall vara förberedda genom en grundinstallation av Debian
Linux version 9.5.  Det skall gå att logga in på dessa med ssh från
den maskin och med den användare som utför installationen.  Användaren
behöver root-rättigheter (eventuellt via sudo).

Exempel på inventory-fil till ansible där användare "ansible"
förutsätts finnas på alla maskiner med ssh-nyckel installerad och
sudo-rättigheter utan att behöva ange lösenord:

    [all:vars]
    ansible_user=ansible
    ansible_become=true
	tpk_domain=tpk-web.example.com
	tpk_additional_domains=
    
    [webservers]
    tpk-web.example.com
    
    [applicationservers]
    tpk-as1.example.com
    tpk-as2.example.com
    
    [databaseservers]
    tpk-db.example.com
    
    [buildservers]
    tpk-build.example.com
    
    [deployserver]
    tpk-build.example.com
    
### Google API-nycklar

En fil google_api_vars.yml behöver skapas med följande variabler:

    places_autocomplete_key: <API nyckel till Google>
    places_key: <API nyckel till Google>
    maps_key: <API nyckel till Google>

### Inloggning till test-miljö

En fil secret_vars.yml med följande variabler behöver skapas:

    deploy_username: <användarnamn>
    deploy_password: <lösenord>

Användaren skall ha administrativa rättigheter på test-miljön.

## Översikt över installationsprocessen

Installationen sker med kommandot:

    ansible-playbook -i inventory --skip-tags dev,schema tandvpris.yml

### Byggd programvara

Programvaran hämtas från gitlab och byggs på byggservern.  De kompilerade filerna hämtas till underkatalogen files/target.  Här genereras även slumpvariabler.  Dessa hämtas till vars/target.


### Databasserver

Installation och konfiguration av PostgreSQL.  Databasanvändare och tom databas skapas till XWiki.

### Applikationsserver

Installation och konfiguration av Apache Tomcat8 och XWiki.

Om flera applikationsservrar angivits förbereds en konfiguration av jgroups för att låta XWiki fungera i ett kluster.

### Webbserver

Installation och konfiguration av Apache httpd.

