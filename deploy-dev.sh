#!/bin/bash -e

. $HOME/scripts/ansible-configuration

ansible_check_role_submodules
ansible_check_role_submodules 1

if [[ -n "$1" ]] ; then
  ansible-playbook -i inventory-dev --skip-tags dev,deploy --start-at-task "$1" tandvpris.yml
else
  ansible-playbook -i inventory-dev --skip-tags dev,deploy tandvpris.yml
fi
