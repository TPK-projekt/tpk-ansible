#!/bin/bash -e

. $HOME/scripts/ansible-configuration

ansible_check_role_submodules
ansible_check_role_submodules 1

if [[ -n "$1" ]] ; then
  ansible-playbook --flush-cache --skip-tags nondev,schema -i test-inventory --start-at-task "$1" tandvpris.yml
else
  ansible-playbook --flush-cache --skip-tags nondev,schema -i test-inventory tandvpris.yml
fi
