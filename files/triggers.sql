CREATE OR UPDATE FUNCTION set_surgery_id_public() RETURNS trigger AS $set_surgery_id$
  DECLARE
    spacename xwikidoc.xwd_web%TYPE
    id        text
  BEGIN
    spacename := (SELECT xwo_web FROM xwikidoc JOIN xwikiobjects ON xwo_name = xwd_fullname where xwo_id = NEW.xwo_id)
    id := (select regexp_replace(spacename, '^Mottagning\.', ''));
    IF id = spacename THEN
        RAISE EXCEPTION 'Refusing to save in space %.', spacename;
    ELSE
        IF TG_ARGV[0] == 'public' THEN
           NEW.tvppc_surgery_id := to_number(id, '99999999999999999999');
        ELSE
           NEW.tvpic_surgery_id := to_number(id, '99999999999999999999');
        RETURN NEW;
    END IF;
  END
$set_surgery_id$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS set_surgery_id_internal;
CREATE TRIGGER set_surgery_id_internal BEFORE INSERT OR UPDATE ON tvpris_internalpricecomment FOR EACH ROW EXECUTE PROCEDURE set_surgery_id('public');
DROP TRIGGER IF EXISTS set_surgery_id_public;
CREATE TRIGGER set_surgery_id_public   BEFORE INSERT OR UPDATE ON tvpris_publicpricecomment   FOR EACH ROW EXECUTE PROCEDURE set_surgery_id('internal');

CREATE OR REPLACE FUNCTION set_surgery_id() RETURNS trigger AS $set_surgery_id$
  DECLARE
    spacename xwikidoc.xwd_web%TYPE;
    id        text;
  BEGIN
    spacename := (SELECT xwd_web FROM xwikidoc JOIN xwikiobjects ON xwo_name = xwd_fullname where xwo_id = NEW.xwo_id);
    id := (select regexp_replace(spacename, \'^Mottagning\\.\', \'\'));
    IF id = spacename THEN
        RAISE EXCEPTION \'Refusing to save in space %.\', spacename;
    ELSE
        IF TG_ARGV[0] = \'public\' THEN
           NEW.tvppc_surgery_id := to_number(id, \'99999999999999999999\');
        ELSE
           NEW.tvpic_surgery_id := to_number(id, \'99999999999999999999\');
        END IF;
        RETURN NEW;
    END IF;
  END;
$set_surgery_id$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS set_surgery_id_internal ON tvpris_internalpricecomment;
CREATE TRIGGER set_surgery_id_internal BEFORE INSERT OR UPDATE ON tvpris_internalpricecomment FOR EACH ROW EXECUTE PROCEDURE set_surgery_id(\'internal\');
DROP TRIGGER IF EXISTS set_surgery_id_public ON tvpris_publicpricecomment;
CREATE TRIGGER set_surgery_id_public   BEFORE INSERT OR UPDATE ON tvpris_publicpricecomment   FOR EACH ROW EXECUTE PROCEDURE set_surgery_id(\'public\');

