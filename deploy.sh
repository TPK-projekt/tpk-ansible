#!/bin/bash -e

. $HOME/scripts/ansible-configuration

ansible_check_role_submodules

if [[ -n "$1" ]] ; then
  ansible-playbook -i inventory --skip-tags dev,install,schema --start-at-task "$1" tandvpris.yml
else
  ansible-playbook -v -i inventory --skip-tags dev,schema tandvpris.yml
  #ansible-playbook -v -i inventory --skip-tags dev,schema --tags deploy tandvpris.yml
fi
